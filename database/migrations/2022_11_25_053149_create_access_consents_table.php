<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessConsentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_consents', function (Blueprint $table) {
            $table->string("id", 64)->primary();
            $table->string("app_id", 64);
            $table->text('abilities');
            $table->integer('token_expire')->nullable();
            $table->string('consent_token', 8)->nullable();
            $table->unsignedBigInteger("user_id")->nullable();
            $table->boolean('consented')->default(false);
            $table->timestamp('consent_expire');
            $table->timestamps();

            $table->foreign('app_id')->references('id')->on('applications');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_consents');
    }
}
