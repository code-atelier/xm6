<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationConsentUrisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_consent_uris', function (Blueprint $table) {
            $table->id();
            $table->string("app_id", 64);
            $table->text('uri');
            $table->boolean('is_default')->default(false);
            $table->boolean('use_ajax')->default(true);
            $table->timestamps();
            
            $table->index('is_default');
            $table->foreign('app_id')->references('id')->on('applications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_consent_uris');
    }
}
