<?php

use App\Models\TenantRoleAbility;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantRoleAbilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_role_abilities', function (Blueprint $table) {
            $table->id();
            $table->enum("role", ["owner", "admin", "member"]);
            $table->string("ability_id", 129);
            $table->timestamps();

            $table->foreign('ability_id')->references('id')->on('abilities');
        });
        $this->initialize_tenant_role_abilities();
    }

    private function initialize_tenant_role_abilities() {
        $records = [
            [
                "role" => "owner",
                "ability_id" => "tenant:manage"
            ],
            [
                "role" => "owner",
                "ability_id" => "tenant:manageApps"
            ],
            [
                "role" => "owner",
                "ability_id" => "tenant:manageMembers"
            ],
            [
                "role" => "owner",
                "ability_id" => "tenant:managePublishedApps"
            ],
            [
                "role" => "owner",
                "ability_id" => "tenant:viewApps"
            ],
            [
                "role" => "owner",
                "ability_id" => "tenant:manageAdmins"
            ],
            [
                "role" => "owner",
                "ability_id" => "tenant:changeOwner"
            ],
            [
                "role" => "admin",
                "ability_id" => "tenant:manageApps"
            ],
            [
                "role" => "admin",
                "ability_id" => "tenant:manageMembers"
            ],
            [
                "role" => "admin",
                "ability_id" => "tenant:viewApps"
            ],
            [
                "role" => "member",
                "ability_id" => "tenant:viewApps"
            ],
        ];

        foreach($records as $rec) {
            TenantRoleAbility::create($rec);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_role_abilities');
    }
}
