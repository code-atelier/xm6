<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_invitations', function (Blueprint $table) {
            $table->id();
            $table->string("token", 16)->unique();
            $table->unsignedBigInteger("tenant_id")->nullable();
            $table->unsignedBigInteger("requesting_user_id")->nullable();
            $table->unsignedBigInteger("requested_user_id")->nullable();
            $table->unsignedBigInteger("tenant_team_id")->nullable();
            $table->enum("invited_as", ["owner", "admin", "member"]);
            $table->string("display_role", 30)->nullable();
            $table->unsignedBigInteger("reports_to")->nullable();
            $table->timestamp("valid_until");
            $table->boolean("accepted")->nullable();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('tenants');
            $table->foreign('requesting_user_id')->references('id')->on('users');
            $table->foreign('requested_user_id')->references('id')->on('users');
            $table->foreign('reports_to')->references('id')->on('tenant_members');
            $table->foreign('tenant_team_id')->references('id')->on('tenant_teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_invitations');
    }
}
