<?php

use App\Models\Application;
use App\Xm6\ExMachina;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->string("id", 64)->primary();
            $table->unsignedBigInteger("owner_id")->nullable();
            $table->unsignedBigInteger("developer_id")->nullable();
            $table->unsignedBigInteger("publisher_id")->nullable();

            $table->string("name", 128);
            $table->string("current_stable_version", 20);
            $table->string("secret_key", 128)->unique()->nullable();
            $table->boolean("is_system")->default(false);
            
            $table->boolean('active')->default(false);
            $table->boolean('verified')->default(false);
            $table->timestamp('verified_at')->nullable();

            $table->timestamps();

            $table->index('name');
            $table->foreign('owner_id')->references('id')->on('users');
            $table->foreign('developer_id')->references('id')->on('tenants');
            $table->foreign('publisher_id')->references('id')->on('tenants');
        });

        $this->initialize_applications();
    }

    private function initialize_applications() {
        $records = [
            [
                'id' => "app.codeatelier.exmachina",
                'owner_id' => null,
                'developer_id' => null,
                'publisher_id' => null,
                'name' => "ExMachina 6",
                'current_stable_version' => ExMachina::$Version,
                'is_system' => true,
                'active' => true,
            ],
        ];

        foreach($records as $rec) {
            Application::create($rec);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
