<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 30)->unique();
            $table->string('given_name', 50);
            $table->string('middle_name', 50)->default("");
            $table->string('surname', 50)->default("");
            $table->string('secret', 64);
            $table->string('email')->unique();
            $table->string('phone', 20)->unique()->nullable();
            $table->string('password');
            $table->enum("two_factor", ["email", "phone", "none"])->default("none");
            $table->string("locale", 5)->default("en-US");
            $table->boolean("surname_first")->default(false);
            $table->date("birthday")->nullable();
            $table->rememberToken();
            $table->boolean('activated')->default(false);
            $table->timestamp('activated_at')->nullable();
            $table->boolean('verified')->default(false);
            $table->timestamp('verified_at')->nullable();
            $table->boolean('banned')->default(false);
            $table->boolean('oauth_bound')->default(false);
            $table->string('oauth_source', 30)->nullable();
            $table->string('oauth_id', 128)->nullable();
            $table->timestamps();

            $table->index(['name', 'email', 'phone']);
            $table->index(["given_name", "middle_name", "surname"]);
            $table->index(['activated', 'verified', 'banned']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
