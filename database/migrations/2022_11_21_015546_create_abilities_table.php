<?php

use App\Models\Ability;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abilities', function (Blueprint $table) {
            $table->string("id", 129)->primary();
            $table->enum('scope', ['tenant', 'application', 'system']);
            $table->string("app_id", 64)->nullable();
            $table->string("display_name", 50);
            $table->string("short_description", 100);
            $table->boolean("sensitive")->default(false);
            $table->timestamps();

            $table->foreign('app_id')->references('id')->on('applications');
        });
        
        $this->initialize_xm6_basic_abilities();
    }

    private function initialize_xm6_basic_abilities() {
        $abilities = [
            [
                "id" => '*',
                "scope" => 'system',
                "display_name" => 'Act On Your Behalf',
                "short_description" => 'act on your behalf, able to do anything you can do',
                "sensitive" => true
            ],
            [
                "id" => 'tenant:manage',
                "scope" => 'tenant',
                "display_name" => 'Manage Tenant',
                "short_description" => 'manage tenant data and information',
                "sensitive" => true
            ],
            [
                "id" => 'tenant:manageMembers',
                "scope" => 'tenant',
                "display_name" => 'Manage Tenant Members',
                "short_description" => 'manage existing tenant members, invite new member, and create new member'
            ],
            [
                "id" => 'tenant:manageApps',
                "scope" => 'tenant',
                "display_name" => 'Manage Tenant Applications',
                "short_description" => 'manage tenant apps and assign app roles to members'
            ],
            [
                "id" => 'tenant:viewApps',
                "scope" => 'tenant',
                "display_name" => 'Manage Tenant Applications',
                "short_description" => 'manage tenant apps and assign app roles to members'
            ],
            [
                "id" => 'tenant:managePublishedApps',
                "scope" => 'tenant',
                "display_name" => 'Manage Published Applications',
                "short_description" => 'manage tenant\'s published apps and publish new app',
                "sensitive" => true
            ],
            [
                "id" => 'tenant:changeOwner',
                "scope" => 'tenant',
                "display_name" => 'Change Tenant Owner',
                "short_description" => 'change tenant owner to other member',
                "sensitive" => true
            ],
            [
                "id" => 'tenant:manageAdmins',
                "scope" => 'tenant',
                "display_name" => 'Manage Tenant Admins',
                "short_description" => 'manage tenant admins, promote member to admin, and demote admin to member',
                "sensitive" => true
            ],
            [
                "id" => 'tenant:view',
                "scope" => 'tenant',
                "display_name" => 'View Tenant',
                "short_description" => 'view tenant information, members and structure'
            ],
            [
                "id" => 'system:viewBasicInfo',
                "scope" => 'system',
                "display_name" => 'View Basic Information',
                "short_description" => 'identify you and view your basic information, including all information you made public'
            ],
            [
                "id" => 'system:viewPersonalInfo',
                "scope" => 'system',
                "display_name" => 'View Personal Information',
                "short_description" => 'identify you and view your basic and personal information, including all information you made public',
                "sensitive" => true
            ],
            [
                "id" => 'system:viewAssociatedTenants',
                "scope" => 'system',
                "display_name" => 'View Associated Tenants',
                "short_description" => 'identify you and view tenants associated with your account',
                "sensitive" => true
            ],
            [
                "id" => 'system:modifyPersonalInfo',
                "scope" => 'system',
                "display_name" => 'Modify Personal Information',
                "short_description" => 'modify your basic and personal information',
                "sensitive" => true
            ],
        ];

        foreach($abilities as $ability) {
            Ability::create($ability);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abilities');
    }
}
