<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_members', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("tenant_id");
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("tenant_team_id")->nullable();
            $table->enum("administrative_role", ["owner", "admin", "member"]);
            $table->string("display_role", 30)->nullable();
            $table->unsignedBigInteger("reports_to")->nullable();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('tenants');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('reports_to')->references('id')->on('tenant_members');
            $table->foreign('tenant_team_id')->references('id')->on('tenant_teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_members');
    }
}
