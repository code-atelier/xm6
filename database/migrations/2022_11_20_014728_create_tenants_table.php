<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->id();
            $table->string("guid", 32)->unique();
            $table->string("name", 128);
            $table->string("description", 255);

            $table->string("address1", 100)->nullable();
            $table->string("address2", 100)->nullable();
            $table->string("city", 100)->nullable();
            $table->string("country", 100)->nullable();

            $table->string("email", 128)->nullable();
            $table->string("line_phone", 28)->nullable();
            $table->string("phone", 28)->nullable();

            $table->boolean("public")->default(true);

            $table->boolean("active")->default(true);

            $table->boolean("verified")->default(false);
            $table->timestamp("verified_at")->nullable();

            $table->integer("member_limit")->default(50);
            $table->boolean("is_developer")->default(false);
            $table->boolean("is_publisher")->default(false);

            $table->unsignedBigInteger("created_by")->nullable();
            $table->timestamps();

            $table->index("name");
            $table->index(["public", "active"]);
            $table->index(["is_developer", "is_publisher"]);

            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
