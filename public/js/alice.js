class DataValidator {
    validationRule = {};
    errors = {};
    validated = null;

    constructor(validationRule) {
        this.validationRule = validationRule || {};
    }

    parseValidationRules(rules) {
        if (typeof rules !== "object")
            return {};
        var parsedRules = {};
        for(var k in rules) {
            var rule = rules[k];
            if (typeof(rule) === "string") {
                rule = rule.split("|");
            } 
            if (Array.isArray(rule)) {
                var rls = [];
                for(var i = 0; i < rule.length; i++) {
                    if (typeof rule[i] === "string") {
                        var spl2 = rule[i].split(":");
                        var ruleId = spl2[0].trim();
                        if (ruleId) {
                            if (!this['validate_' + ruleId]) {
                                throw new Error("'" + ruleId + "' is not a valid validation rule at path '" + k + "'")
                            }
                            var ruleParams = spl2[1];
                            if (ruleParams) {
                                ruleParams = ruleParams.split(",");
                                for(var w = 0; w < ruleParams.length; w++) {
                                    ruleParams[w] = ruleParams[w].trim();
                                }
                            }
                            rls.push({
                                rule: ruleId,
                                params: ruleParams,
                            });
                        }
                    } else if (typeof rule[i] === "object" && rule[i].rule) {
                        rls.push(rule[i]);
                    } else {
                        throw new Error("Invalid rule datatype: " + (typeof rule[i]));
                    }
                }
                parsedRules[k] = rls;
            } else {
                throw new Error("Invalid validation rule");
            }
        }
        return parsedRules;
    }

    castType(value, ruleIds) {
        if (value === undefined || value === null) return null;
        if (ruleIds.includes("number"))
            return Number(value) || 0;
        return value;
    }

    validate(data, validationRule) {
        this.errors = {};
        this.validated = null;
        var hasError = false;
        var validated = {};
        validationRule = this.parseValidationRules(validationRule || this.validationRule);

        for (var field in validationRule) {
            var value = data[field];
            var rules = validationRule[field];
            var ruleIds = [];
            for(var i = 0; i < rules.length; i++) {
                if (rules[i].ruleId)
                    ruleIds.push(rules[i].ruleId);
            }
            if (rules.length == 0) {
                validated[field] = value;
            }
            else for(var i = 0; i < rules.length; i++) {
                var rule = rules[i];
                var validationFunction = "validate_" + rule.rule;
                if (typeof this[validationFunction] == "function") {
                    var res = this[validationFunction](value, field, data, rule.params, rules, ruleIds);
                    if (res !== true) {
                        this.errors[field] = (res || "{field} contains invalid value").replace("{field}", field);    
                        hasError = true;                    
                    } else {
                        validated[field] = this.castType(value, ruleIds);
                    }
                }
            }
        }

        if (!hasError) {
            this.validated = validated;
            return true;
        }
        return false;
    }

    validate_required(v, f, d, p, r, ri) {
        if (v === undefined || v === null)
            return "{field} is required";
        if (!ri.includes("number") && v.trim() == "")
            return "{field} is required";
        return true;
    }

    validate_notempty(v, f, d, p, r, ri) {
        if (v.trim() == "")
            return "{field} cannot be empty";
        return true;
    }

    validate_alphanumeric(v) {
        const regex = /^[0-9a-zA-Z]*$/;
        return typeof v === "string" ? regex.test(v) : "{field} must be alphanumeric";
    }

    validate_email(v) {
        const regex = /^((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/;
        return typeof v === "string" ? regex.test(v) : "{field} must be an email";
    }

    validate_string(v) {
        return typeof v === "string" ? true : "{field} must be a string";
    }

    validate_number(v) {
        return Number(v) !== NaN ? true : "{field} must be a number";
    }

    validate_min(v, f, d, p, r, ri) {
        if (v === undefined || v === null)
            return true;
        var len = Number(length) || 0;
        if (ri.includes("number")) {
            if (Number(v) < len)
                return "{field} must be greater than or equal to " + len;
        } else if (("" + v).trim().length < len)
            return "{field} must have at least " + len + " characters";
        return true;
    }

    validate_samewith(v, f, d, p, r, ri) {
        var sameFieldName = p[0] ? p[0].trim() : null;
        if (!sameFieldName)
            throw new Error("samewith validation expects a parameter");
        var sameWith = d[sameFieldName];
        if (v != sameWith) {
            return "{field} must have the same value with " + sameFieldName;
        }
        return true;
    }
}