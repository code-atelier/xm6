<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>@yield('title', env('SITE_NAME'))</title>

        @if (!isset($useBase) || $useBase == true)
        <base href="{{ env('APP_URL') }}/" />
        @endif

        <!-- Font Awesome -->
        <link href="fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="fontawesome/css/solid.min.css" rel="stylesheet">

        <!-- Site CSS -->
        <link href="css/app.css" rel="stylesheet">
        @yield('styles', '')

        <!-- Scripts -->
        <script src="js/alice.js"></script>
        @yield('headscripts', '')
    </head>
    <body class="@yield('bodyclass', '')">
        <!-- Header/Nav -->

        <!-- Content -->
        <main class="@yield('mainclass', 'container')">
            @yield('content')
        </main>

        <!-- Footer -->

        <!-- Scripts -->
        @yield('bodyscripts', '')
    </body>
</html>
