<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Laravel</title>

        <base href="{{ env('APP_URL') }}/" />

        <!-- Font Awesome -->
        <link href="fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="fontawesome/css/solid.min.css" rel="stylesheet">

        <link href="css/app.css" rel="stylesheet">
    </head>
    <body>
        <header class="p-3 text-bg-dark">
            <div class="container">
              <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="./" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
                  <img src="webicon.png" title="icon" class="mx-2" height="32" />
                </a>
        
                <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                  <li><a href="#" class="nav-link px-2 text-secondary fw-bold">Home</a></li>
                  <li><a href="#" class="nav-link px-2 text-white">Products</a></li>
                  <li><a href="#" class="nav-link px-2 text-white">About</a></li>
                  <li><a href="#" class="nav-link px-2 text-white">Privacy Policy</a></li>
                </ul>
        
                <div class="text-end">
                    <a href="./signin"><button type="button" class="btn btn-outline-light me-2">Sign in</button></a>
                    <a href="./register"><button type="button" class="btn btn-outline-primary">Register</button></a>
                </div>
              </div>
            </div>
        </header>
        
        <main id="app">
            <div class="container col-xxl-8 px-4 py-5 bg-blue-100">
                <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
                  <div class="col-10 col-sm-8 col-lg-6">
                    <img src="bootstrap-themes.png" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
                  </div>
                  <div class="col-lg-6">
                    <h1 class="display-5 fw-bold lh-1 mb-3">Software Engineering and Development Expert</h1>
                    <p class="lead">Quickly design and customize responsive mobile-first sites with Bootstrap, the world’s most popular front-end open source toolkit, featuring Sass variables and mixins, responsive grid system, extensive prebuilt components, and powerful JavaScript plugins.</p>
                    <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                      <button type="button" class="btn btn-primary btn-lg px-4 me-md-2">Primary</button>
                      <button type="button" class="btn btn-outline-secondary btn-lg px-4">Default</button>
                    </div>
                  </div>
                </div>
            </div>
            <example-component></example-component>
        </main>

        <script src="{{ env('APP_URL') }}{{ mix('/js/app.js') }}"></script>
    </body>
</html>
