@extends('layouts/minimalistic')

@section('title', env('SITE_NAME') . ' Account Registration')

@section('styles')
<link href="css/register.css" rel="stylesheet">    
@endsection

@section('headscripts')
<script src="https://accounts.google.com/gsi/client" async defer></script>
@endsection

@section('bodyscripts')
<script src="{{ env('APP_URL') }}{{ mix('/js/app.js') }}"></script>
<script>
    function validate(data) {
        var validator = new DataValidator({
            "name": "required"
        });
        validator.validate(data);
    }
</script>
@endsection

@section('content')
<div class="card my-5 p-3">
    <div class="card-body">
        <div class="">
            <img src="res/weblogo_light.png" class="d-block mb-2" />
            <h2>Create Your {{env('SITE_NAME')}} Account</h2>
        </div>
        
        <div id="app" class="row g-5">
            <div class="col-lg-8 col-12">
                <validated-form target="{{env('APP_URL')}}/api/auth/register" method="post" ajax validator="validate">
                    <input type="hidden" name="oauth_source" value="{{isset($oauth) ? $oauth['source'] : ''}}" />
                    <input type="hidden" name="oauth_id_token" value="{{isset($oauth) ? $oauth['id_token'] : ''}}" />
                    <form-input id="username" field="username" label="Username" group="@" placeholder="username" type="text" prefix="@" required
                        error="Username is required" validation="required|min:5|alphanumeric">
                    </form-input>
                    <form-input id="given_name" field="given_name" label="Given Name" size="md-4" type="text" value="{{isset($oauth) ? $oauth['given_name'] : ''}}"  required 
                        error="Given name is required" validation="required|min:3">
                    </form-input>
                    <form-input id="middle_name" field="middle_name" label="Middle Name" size="md-4" type="text" value="{{isset($oauth) ? $oauth['middle_name'] : ''}}">
                    </form-input>
                    <form-input id="surname" field="surname" label="Surname" size="md-4" type="text" value="{{isset($oauth) ? $oauth['surname'] : ''}}">
                    </form-input>
                    <form-input id="email" field="email" label="Email" size="7" type="email" placeholder="you@example.com" value="{{isset($oauth) ? $oauth['email'] : ''}}" required
                        validation="required|email">
                    </form-input>
                    <form-input id="birthday" field="birthday" label="Birthday" size="5" type="date" value="{{isset($oauth) ? $oauth['birthday'] : ''}}">
                    </form-input>
                    <form-input id="password" field="password" label="Password" size="6" type="password" placeholder="Password" autocomplete="new-password" required 
                        validation="required|min:8">
                    </form-input>
                    <form-input id="confirm_password" field="confirm_password" label="Confirm Password" size="6" type="password" autocomplete="new-password" placeholder="Repeat Password" required
                        validation="required|min:8|samewith:password">
                    </form-input>
                    
                    <hr class="my-4">

                    <form-checkbox class="mt-0" field="tos_agreed" id="tos_agreed">I have read and agree with the <a href="./tos" target="_blank">terms of service</a>.</form-checkbox>
                    <form-checkbox class="mt-0" field="privacy_agreed" id="privacy_agreed">I have read and agree with <a href="./privacy" target="_blank">privacy policy</a>.</form-checkbox>

                    <hr class="my-4">

                    <div class="row">
                        <div class="col-lg-3">
                            <button class="w-100 btn btn-sm btn-primary btn-lg" type="submit">Create Account</button>
                        </div>
                        <div class="col-md-4 col-lg-3 pt-2 pt-lg-0">
                            <a href="signin" class="fw-semi-bold d-inline-block pt-md-1" style="font-size: 1.0rem; text-decoration: none">Sign in instead</a>
                        </div>
                        @if (!isset($oauth))
                        <div class="col-md-8 col-lg-6 pt-2 pt-lg-0">
                            <div id="g_id_onload"
                                data-client_id="{{env('OAUTH_GOOGLE_CLIENT_ID')}}"
                                data-login_uri="{{env('APP_URL')}}/oauth/register/google"
                                data-auto_prompt="false">
                            </div>
                            <div class="g_id_signin"
                                data-type="standard"
                                data-size="medium"
                                data-theme="outline"
                                data-text="continue_with"
                                data-shape="rectangular"
                                data-logo_alignment="left">
                            </div>
                        </div>
                        @else
                        <div class="col-md-8 col-lg-6 pt-2 pt-lg-0">
                            <div class="pt-md-1 text-muted">
                                <i class="fa fa-solid fa-user-lock"></i>
                                Connected to {{$oauth['source']}} account.
                            </div>
                        </div>
                        @endif
                    </div>
                </validated-form>
            </div>
            <div class="col-lg-4 d-none d-lg-block text-center">
                <div class="mt-2">
                    <img src="res/xm6_200.png" />
                    <p class="mt-4" style="font-size: larger">Powered by ExMachina 6</p>
                    <p class="mt-0"><a href="./" style="text-decoration: none">Return to home</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
