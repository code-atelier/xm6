<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Sign In</title>

        <base href="{{ env('APP_URL') }}/" />

        <!-- Font Awesome -->
        <link href="fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="fontawesome/css/solid.min.css" rel="stylesheet">

        <link href="css/app.css" rel="stylesheet">
        <link href="css/signin.css" rel="stylesheet">
    </head>
    <body class="text-center text-light">
        <main class="form-signin w-100 m-auto">
          <form>
            <i class="d-block fa-solid fa-user-lock mb-3" style="font-size: 48px"></i>
            <h1 class="h3 mb-3 fw-normal">ExMachina Secure Signin</h1>
        
            <div class="form-floating text-dark">
              <input type="text" class="form-control" id="floatingInput" placeholder="johndoe">
              <label for="floatingInput">Username or Email</label>
            </div>
            <div class="form-floating text-dark">
              <input type="password" class="form-control" id="floatingPassword" placeholder="Password">
              <label for="floatingPassword">Password</label>
            </div>
        
            <div class="checkbox mb-3">
              <label>
                <input type="checkbox" value="remember-me"> Remember me
              </label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
            <div class="mt-2">
              <a class="text-light" href="./">Back to home</a>
            </div>
            <p class="mt-5 mb-3 text-light">Code Atelier © 2022</p>
          </form>
        </main>
    </body>
</html>
