<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserRegistrationController;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/register', [UserRegistrationController::class, 'register']);
Route::post('/oauth/register/google', [UserRegistrationController::class, 'registerGoogleOAuth']);