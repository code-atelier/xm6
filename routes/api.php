<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/auth/register', [AuthController::class, 'register']);
Route::post('/auth/createusertoken', [AuthController::class, 'createusertoken']);
Route::post('/auth/createconsent', [AuthController::class, 'createconsent']);
Route::post('/auth/createaccesstoken', [AuthController::class, 'createaccesstoken']);

Route::get('/oauth/google/register', [AuthController::class, 'googleoauthregister']);

Route::middleware(['auth:sanctum'])->prefix('auth')->group(function() {
    Route::get('/', [AuthController::class, 'index']);
    Route::post('/refreshusertoken', [AuthController::class, 'refreshusertoken']);
    Route::get('/consent/{consentId}', [AuthController::class, 'getconsent']);
    Route::post('/consent/{consentId}', [AuthController::class, 'submitconsent']);
    Route::post('/submitconsent', [AuthController::class, 'submitconsent']);
});