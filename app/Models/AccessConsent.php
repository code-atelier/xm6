<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessConsent extends Model
{
    use HasFactory;
    
    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'app_id',
        'abilities',
        'token_expire',
        'consent_token',
        'user_id',
        'consented',
        'consent_expire',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'consent_expire' => 'datetime',
        'token_expire' => 'integer',
    ];

    public function application() {
        return $this->belongsTo(Application::class, 'app_id');
    }

    public function hasExpired() {
        $this->consent_expire->lte(now());
    }
}
