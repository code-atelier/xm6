<?php

namespace App\Models;

use Exception;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Mockery\CountValidator\Exact;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'given_name',
        'middle_name',
        'surname',
        'email',
        'phone',
        'two_factor',
        'surname_first',
        'locale',
        'birthday',
        'password',

        'secret',
        'activated',
        'activated_at',
        'verified',
        'verified_at',
        'banned',
        'oauth_bound',
        'oauth_source',
        'oauth_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'secret',
        'oauth_bound',
        'oauth_source',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'verified_at' => 'datetime:Y-m-d H:i:s',
        'activated_at' => 'datetime:Y-m-d H:i:s',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'surname_first' => 'boolean',
        'activated' => 'boolean',
        'verified' => 'boolean',
        'banned' => 'boolean',
    ];

    protected $appends = [
        'full_name'
    ];

    public function getFullNameAttribute() {
        return $this->surname_first ? 
            (trim($this->surname . (trim($this->middle_name) != "" ? " " . $this->middle_name : "") . " " . $this->given_name))
            : (trim($this->given_name . (trim($this->middle_name) != "" ? " " . $this->middle_name : "") . " " . $this->surname));
    }

    public function ableTo(mixed $abilities) {
        if (!is_array($abilities)) {
            $abilities = [$abilities];
        }
        foreach($abilities as $ability) {
            if (!is_string($ability)) {
                return false;
            }
            $spl = explode("@", $ability);
            
            $abilityId = $spl[0];
            $tenantId = isset($spl[1]) ? $spl[1] : null;
            $tenant = null;
            if (isset($tenantId)) {
                $tenant = Tenant::where('guid', $tenantId)->first();
            }

            $a = Ability::where('id', $abilityId)->first();
            if (isset($a)) {
                if ($a->scope == 'tenant') {
                    if (!isset($tenant) || !$tenant->active) {
                        return false;
                    }
                    $member = TenantMember::where('tenant_id', $tenant->id)->where('user_id', $this->id)->first();

                    if ($a->id == "tenant:view") {
                        // if a member of this tenant, or tenant is public
                        if (isset($member) || $tenant->public) {
                            continue;
                        }
                    }

                    if (!isset($member)) return false;
                    $roleAbility = TenantRoleAbility::where("role", $member->administrative_role)->where("ability_id", $abilityId)->first();
                    if (!isset($roleAbility)) {
                        return false;
                    }
                }

                // check whether the user can do it
                throw new Exception("Not Implemented");
            } else {
                return false;
            }
        }
        return true;
    }

    public function sendActivationEmail() {
        if ($this->active) {
            return "User is already active";
        }
        if ($this->banned) {
            return "User is banned";
        }
        // sendmail

        return true;
    }
}
