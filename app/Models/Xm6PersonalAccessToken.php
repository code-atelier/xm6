<?php

namespace App\Models;

use Laravel\Sanctum\PersonalAccessToken;

class Xm6PersonalAccessToken extends PersonalAccessToken
{
    protected $table = 'personal_access_tokens';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'abilities' => 'json',
        'last_used_at' => 'datetime',
        'expire' => 'integer',
        'revalidate' => 'integer',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'token',
        'abilities',
        'expire',
        'revalidate',
    ];

    public function hasExpired() {
        if (!isset($this->expire)) return false;
        return $this->created_at->lt(now()->subSeconds($this->expire));
    }
    public function canRefresh() {
        if (!isset($this->revalidate) || !isset($this->expire)) return false;
        return $this->created_at->gt(now()->subSeconds($this->revalidate));
    }
}
