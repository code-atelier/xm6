<?php

namespace App\Http\Controllers;

use App\Models\Ability;
use App\Models\AccessConsent;
use App\Models\Application;
use App\Models\User;
use App\Xm6\Exceptions\InvalidModelStateException;
use App\Xm6\Services\Users\UserService;
use Illuminate\Container\EntryNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException;
use Nette\Utils\Random;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Laravel\Socialite\Facades\Socialite;
use Throwable;

class AuthController extends Controller
{
    private $registration_validation_rules = [
        'name' => 'required|string|min:5|max:30|alpha_num|unique:App\Models\User,name',
        'given_name' => 'required|string|max:50',
        'middle_name' => 'string|max:50|nullable',
        'surname' => 'string|max:50|nullable',
        'email' => 'required|email|unique:App\Models\User,email',
        'phone' => 'string|max:50',
        'two_factor'=> 'in:email,phone,none|nullable',
        'surname_first' => "boolean|nullable",
        'locale' => "string|max:5",
        'birthday' => "date",
        'password' => 'required|string|min:8',
        'confirm_password' => 'required|string|same:password',
    ];

    private $create_consent_validation_rules = [
        'abilities' => 'required|array',
        'expire' => 'integer|min:0|nullable',
        'app_id' => 'required|string|exists:App\Models\Application,id',
        'secret_key' => 'required|string'
    ];

    private $createaccesstoken_validation_rules = [
        'consent_id' => 'required|string',
        'consent_token' => 'required|string'
    ];

    private $createusertoken_validation_rules = [
        'identity' => 'required|string',
        'password' => 'required|string|min:8',
    ];

    // == Common
    public function index(Request $request) {
        try {
            if (Auth::check()) {
                $user = Auth::user();
                $res = UserService::getTokenInfo($user);
                return $this->handleSuccess($res);
            } else {
                throw new UnauthorizedException();
            } 
        } catch(UnauthorizedException $br) {
            return $this->handleUnauthorized($br->getMessage());
        } catch(Throwable $ex) {
            return $this->handleInternalServerError();
        }
    }

    public function register(Request $request)
    {
        try{
            $input = $this->validateInput($request->all(), $this->registration_validation_rules);
            $res = UserService::registerUser($input);
            return $this->handleSuccess($res);
        } catch(InvalidModelStateException $br) {
            return $this->handleBadRequest($br->getMessage(), $br->getErrors());
        } catch(Throwable $ex) {
            return $this->handleInternalServerError();
        }
    }

    // == User Token
    public function createusertoken(Request $request)
    {
        try {
            $input = $this->validateInput($request->all(), $this->createusertoken_validation_rules);
            $res = UserService::createUserToken($input);
            return $this->handleSuccess($res);
        } catch(InvalidModelStateException $br) {
            return $this->handleBadRequest($br->getMessage(), $br->getErrors());
        } catch(UnauthorizedException $br) {
            return $this->handleUnauthorized($br->getMessage());
        } catch(Throwable $ex) {
            return $this->handleInternalServerError();
        }
    }

    public function refreshusertoken() {
        try {
            if (Auth::check()) {
                $user = Auth::user();
                $res = UserService::refreshUserToken($user);
                return $this->handleSuccess($res);
            } else {
                throw new UnauthorizedException();
            } 
        } catch(BadRequestException $br) {
            return $this->handleBadRequest($br->getMessage());
        } catch(UnauthorizedException $br) {
            return $this->handleUnauthorized($br->getMessage());
        } catch(Throwable $ex) {
            return $this->handleInternalServerError();
        }
    }

    // == Access Token
    public function createconsent(Request $request) {
        try {
            $input = $this->validateInput($request->all(), $this->create_consent_validation_rules);
            $res = UserService::createConsent($input);
            return $this->handleSuccess($res);
        } catch(InvalidModelStateException $br) {
            return $this->handleBadRequest($br->getMessage(), $br->getErrors());
        } catch(UnauthorizedException $br) {
            return $this->handleUnauthorized($br->getMessage());
        } catch(Throwable $ex) {
            return $this->handleInternalServerError();
        }
    }

    public function getconsent(Request $request, $consentId) {
        try {
            if (Auth::check()) {
                $res = UserService::getConsent($consentId);
                return $this->handleSuccess($res);
            } else {
                throw new UnauthorizedException();
            } 
        } catch(BadRequestException $br) {
            return $this->handleBadRequest($br->getMessage());
        } catch(EntryNotFoundException $br) {
            return $this->handleNotFound($br->getMessage());
        } catch(UnauthorizedException $br) {
            return $this->handleUnauthorized($br->getMessage());
        } catch(Throwable $ex) {
            return $this->handleInternalServerError();
        }
    }

    public function submitconsent(Request $request, $consentId) {
        try {
            if (Auth::check()) {
                $user = Auth::user();
                $res = UserService::submitConsent($user, $consentId);
                return $this->handleSuccess($res);
            } else {
                throw new UnauthorizedException();
            } 
        } catch(BadRequestException $br) {
            return $this->handleBadRequest($br->getMessage());
        } catch(EntryNotFoundException $br) {
            return $this->handleNotFound($br->getMessage());
        } catch(UnauthorizedException $br) {
            return $this->handleUnauthorized($br->getMessage());
        } catch(Throwable $ex) {
            return $this->handleInternalServerError();
        }
    }

    public function createaccesstoken(Request $request)
    {
        try {
            $input = $this->validateInput($request->all(), $this->createaccesstoken_validation_rules);
            $res = UserService::createAccessToken($input);
            return $this->handleSuccess($res);
        } catch(BadRequestException $br) {
            return $this->handleBadRequest($br->getMessage());
        } catch(EntryNotFoundException $br) {
            return $this->handleNotFound($br->getMessage());
        } catch(UnauthorizedException $br) {
            return $this->handleUnauthorized($br->getMessage());
        } catch(Throwable $ex) {
            return $this->handleInternalServerError();
        }
    }
    
    // == Google OAuth
    public function googleoauthregister(Request $request) {
        try {
            $googleUser = Socialite::driver('google')->redirectUrl(env('APP_URL'). '/api/oauth/google/register')->stateless()->user();
            var_dump($googleUser);
            $res = UserService::registerOauth("google", $googleUser->user);
            return $this->handleSuccess($res);
        } catch(BadRequestException $br) {
            return $this->handleBadRequest($br->getMessage());
        } catch(EntryNotFoundException $br) {
            return $this->handleNotFound($br->getMessage());
        } catch(UnauthorizedException $br) {
            return $this->handleUnauthorized($br->getMessage());
        } catch(Throwable $ex) {
            return $this->handleInternalServerError();
        }
    }
}
