<?php

namespace App\Http\Controllers;

use App\Xm6\Exceptions\InvalidModelStateException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function validateInput($input, $validationRules) {
        $validator = Validator::make($input, $validationRules);
        if($validator->fails()){
            throw new InvalidModelStateException(null, $validator->errors()->toArray());
        }
        return $validator->validated();
    }

    protected function handleBadRequest(string $message = null, array $errors = null) {
        $message = isset($message) && trim($message) != "" ? $message : "Bad Request";
        return $this->handleError(400, $message, $errors);
    }

    protected function handleUnauthorized(string $message = null) {
        $message = isset($message) && trim($message) != "" ? $message : "Unauthorized";
        return $this->handleError(401, $message);
    }

    protected function handleInternalServerError(string $message = null) {
        $message = isset($message) && trim($message) != "" ? $message : "Internal Server Error";
        return $this->handleError(500, $message);
    }

    protected function handleNotFound(string $message = null) {
        $message = isset($message) && trim($message) != "" ? $message : "Not Found";
        return $this->handleError(404, $message);
    }

    protected function handleError(int $statusCode, string $message, array $errors = null) {
        $res = [
            "message" => $message
        ];
        if (isset($errors))
            $res["errors"] = $errors;
        return response()->json($res, $statusCode);
    }

    protected function handleSuccess($data = null) {
        if (!isset($data)) {
            return response('', 204);
        }
        return response()->json($data, 200);
    }

    protected function viewError(string $errorCode, string $message = null, string $returnUrl = null) {
        if (!isset($message)) {
            $message = "Oops, something went wrong. Please try again in few minutes. If the problem persists, please contact the web admin.";
        }
        return $message;
    }
}
