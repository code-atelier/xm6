<?php

namespace App\Http\Controllers;

use App\Xm6\Services\OAuth\GoogleOAuthService;
use Illuminate\Http\Request;
use Throwable;

class UserRegistrationController extends Controller
{
    public function register() {
        return view('register');
    }

    public function registerGoogleOAuth(Request $request) {
        try {
            $input = $request->post();
            $payload = GoogleOAuthService::validateIDToken($input['credential']);
            $data = [
                'source' => 'Google',
                'id_token' => $input['credential'],
                'given_name' => $payload['given_name'],
                'middle_name' => '',
                'surname' => $payload['family_name'],
                'email' => $payload['email'],
                'birthday' => ''
            ];
            return view('register', ['oauth' => $data]);
        }
        catch (Throwable $ex) {
            return $this->viewError("GOOGLE.REGISTRATION_FAILURE", null, '/register');
        }
    }
}
