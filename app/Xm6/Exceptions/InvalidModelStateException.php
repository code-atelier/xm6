<?php

namespace App\Xm6\Exceptions;

use Exception;

class InvalidModelStateException extends Exception
{
    private $modelStateErrors = null;

    public function __construct($message = null, $errors = null)
    {
        if (!isset($message) || trim($message) == "") {
            $message = "Invalid Model State";
        }
        $this->modelStateErrors = $errors;
        parent::__construct($message, 400);
    }

    public function getErrors() {
        return $this->modelStateErrors;
    }
}