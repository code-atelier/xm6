<?php

namespace App\Xm6\IO;

class Converter
{
    public static function base64UrlEncode($text)
    {
        return str_replace(
            ['+', '/', '='],
            ['-', '_', ''],
            base64_encode($text)
        );
    }
    public static function base64UrlDecode($text)
    {
        return base64_decode(str_replace(
            ['-', '_', ''],
            ['+', '/', '='],
            $text
        ));
    }
}