<?php

namespace App\Xm6\Services\OAuth;

use App\Xm6\IO\Converter;
use App\Xm6\Services\Xm6Service;
use Illuminate\Validation\UnauthorizedException;

class GoogleOAuthService extends Xm6Service
{
    public static function validateIDToken(string $idToken, string $csrfToken = null) {
        $client = new \Google\Client(['client_id' => env('OAUTH_GOOGLE_CLIENT_ID')]);
        $payload = $client->verifyIdToken($idToken);
        if ($payload) {
            return $payload;
        } else {
            return false;
        }
    }
}