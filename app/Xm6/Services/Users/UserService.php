<?php

namespace App\Xm6\Services\Users;

use App\Models\Ability;
use App\Models\AccessConsent;
use App\Models\Application;
use App\Models\User;
use App\Xm6\Exceptions\InvalidModelStateException;
use App\Xm6\Services\Xm6Service;
use Illuminate\Container\EntryNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Validation\UnauthorizedException;
use Nette\Utils\Random;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class UserService extends Xm6Service
{
    public static function getTokenInfo(User $user) {
        $res = [
            "full_name" => $user->full_name,
        ];

        $at = $user->currentAccessToken();
        if (isset($at)) {
            $res = UserService::createTokenInfo($at, $res);
        } else throw new UnauthorizedException();

        return $res;
    }

    public static function registerUser($input) {
        if (!isset($input["middle_name"]) || $input["middle_name"] == "")
            unset($input["middle_name"]);
        if (!isset($input["surname"]) || $input["surname"] == "")
            unset($input["surname"]);
        $input['secret'] = Random::generate(64);
        $input['password'] = bcrypt($input['password'] . $input['secret'] . env("CRYPTO_STATIC_SALT"));
        $input['activated'] = !boolval(env('USER_MUST_ACTIVATE', true));
        $user = User::create($input);
        $user = User::where("email", $input['email'])->first();
        
        $res = [
            "message" => "Registration success",
            "user" => [
                "id" => $user->id,
                "name" => $user->name,
                "full_name" => $user->full_name,
            ]
        ];
        $sendMail = $user->sendActivationEmail();
        $res["activation_email_sent"] = $sendMail === true;
   
        return $res;
    }

    public static function createUserToken($input) {
        $user = User::where('name', $input['identity'])->orWhere('email', $input['identity'])->orWhere('phone', $input['identity'])->first();
        if (!isset($user)) {
            throw new UnauthorizedException();
        }
        if (!$user->activated) {
            throw new UnauthorizedException("User is not active");
        }
        if ($user->banned) {
            throw new UnauthorizedException("User is banned");
        }

        $password = $input['password'] . $user->secret . env("CRYPTO_STATIC_SALT");

        $app_id = "XM6TKN";
        if(Auth::attempt(['email' => $user->email, 'password' => $password, 'activated' => 1, 'banned' => 0])) { 
            $auth = Auth::user(); 
            $at = $auth->createToken($app_id);
            $at->accessToken->update([
                'expire' => intval(env("USER_TOKEN_EXPIRE", 3600 * 24 * 3)),
                'revalidate' => intval(env("USER_TOKEN_REFRESH_PERIOD", 3600 * 24 * 30)),
            ]);
            $success['token'] =  $at->plainTextToken; 
            $success['name'] =  $auth->name;
            $success['full_name'] =  $auth->full_name;
            $success = UserService::createTokenInfo($at->accessToken, $success);
   
            return $success;
        } 
        else { 
            throw new UnauthorizedException();
        } 
    }

    public static function refreshUserToken(User $user) {
        $res = [
            "full_name" => $user->full_name,
        ];
        $at = $user->currentAccessToken();
        if (isset($at) && $at) {
            if ($at->canRefresh()) {
                $at->update([
                    'expire' => min($at->revalidate, now()->addSeconds(intval(env("USER_TOKEN_EXPIRE", 3600 * 24 * 3)))->timestamp - Date::parse($at->created_at)->timestamp)
                ]);
                $res = UserService::createTokenInfo($at, $res);
            } else throw new BadRequestException("This token cannot be refreshed");
        } else 
            throw new UnauthorizedException();
        
        return $res;
    }

    public static function createConsent($input) {
        $app = Application::where('id', $input['app_id'])->first();
        if (!isset($app) || $app->is_system || !$app->active) {
            throw new InvalidModelStateException("Invalid Model State", ['app_id' => ["Application is not found or invalid"]]);
        }
        if ($app->secret_key != $input['secret_key']) {
            throw new UnauthorizedException("Invalid secret key");
        }

        $spl = $input['abilities'];
        $abilities_stack = [];
        $invalid = [];
        foreach($spl as $id) {
            $id = trim($id);
            
            $a = Ability::where('id', $id)->first();
            if (isset($a)) {
                if ($a->scope == "tenant") {
                    $invalid[] = $id;
                    continue;
                }
                if ($id == "*") {
                    $abilities_stack = ["*"];
                    $invalid = [];
                    break;
                }
                else {
                    $abilities_stack[] = $id;
                }
            } else {
                $invalid[] = $id;
            }
        }

        if (count($invalid) > 0) {
            $err = [];
            foreach($invalid as $inv) {
                $err[$inv] = [ "not a valid ability" ];
            }
            throw new InvalidModelStateException("Requested abilities are not valid", $err);
        }

        // store consent manifest and token
        $data = [
            'id' => Random::generate(64),
            'app_id' => $app->id,
            'abilities' => implode(',', $abilities_stack),
            'token_expire' => $input['expire'],
            'consent_expire' => now()->addSeconds(5 * 60),
        ];
        AccessConsent::create($data);
        return [
            "consent_id" => $data['id'],
            'consent_expire' => $data['consent_expire'],
        ];
    }

    public static function getConsent(string $consent_id = null) {
        if (!isset($consent_id)) {
            throw new BadRequestException("id parameter is required");
        }
        $consent = AccessConsent::with('application')
            ->where('id', $consent_id)
            ->where('consented', false)->first();
        if (!isset($consent)) {
            throw new EntryNotFoundException("Consent not found");
        }
        if ($consent->hasExpired()) {
            throw new BadRequestException("Consent has expired");
        }

        $abilities = [];
        $spl_abi = explode(",", $consent->abilities);
        foreach($spl_abi as $abi) {
            $ability = Ability::where('id', $abi)->first();
            if (isset($ability))
                $abilities[] = [
                    "id" => $ability->id,
                    "display_name" => $ability->display_name,
                    "short_description" => $ability->short_description,
                    "sensitive" => $ability->sensitive,
                ];
        }

        $res = [
            "application" => $consent->application,
            "abilities" => $abilities,
            "token_expire" => $consent->token_expire,
            "consent_expire" => $consent->consent_expire,
        ];
        return $res;
    }

    public static function submitConsent(User $user, string $consent_id = null) {
        if (!isset($consent_id)) {
            throw new BadRequestException("id parameter is required");
        }
        $consent = AccessConsent::with('application')
            ->where('id', $consent_id)
            ->where('consented', false)->first();
        if (!isset($consent)) {
            throw new EntryNotFoundException("Consent not found");
        }
        if ($consent->hasExpired()) {
            throw new BadRequestException("Consent has expired");
        }

        $consent->update([
            'user_id' => $user->id,
            'consented' => true,
            'consent_token' => Random::generate(8),
        ]);
        $consent = AccessConsent::with('application')
        ->where('id', $consent_id)->first();
        
        $res = [
            "token" => $consent->consent_token,
            "token_expire" => $consent->token_expire,
            "consent_expire" => $consent->consent_expire,
        ];
        return $res;
    }

    public static function createAccessToken($input) {
        $consent = AccessConsent::where('id', $input['consent_id'])
            ->where('consent_token', $input['consent_token'])
            ->where('consented', true)->first();
        if (!isset($consent)) {
            throw new EntryNotFoundException("Consent not found");
        }
        if ($consent->hasExpired()) {
            throw new BadRequestException("Consent has expired");
        }
        $user = User::where('id', $consent->user_id)->first();
        if (!isset($user)) {
            throw new BadRequestException("Bad consent");
        }

        $at = $user->createToken($consent->app_id);
        $at->accessToken->update([
            'expire' => $consent->expire,
        ]);
        $success['token'] =  $at->plainTextToken;
        $success['user_name'] =  $user->name;
        $success['user_full_name'] =  $user->full_name;
        $success = UserService::createTokenInfo($at->accessToken, $success);

        $consent->delete();

        return $success;
    }

    public static function registerOauth($oauthSource, $user) {
        $secret = Random::generate(64);
        $randomPassword = Random::generate(64);
        $data = [
            "name" => "oauth:" . $user["sub"] . "@" . $oauthSource,
            "given_name" => $user["given_name"],
            "surname" => $user["family_name"],
            "email" => $user["email"],
            'secret' => $secret,
            'password' => bcrypt($randomPassword . $secret . env("CRYPTO_STATIC_SALT")),
            "activated" => $user["email_verified"] || !boolval(env('USER_MUST_ACTIVATE', true)),
            'oauth_bound' => true,
            'oauth_source' => $oauthSource,
            'oauth_id' => $user["sub"],
        ];

        $existing = User::where("name", $data["name"])->orWhere(
            function($qry) use ($oauthSource, $user) {
                $qry->where("oauth_source", $oauthSource)
                ->where("oauth_id", $user["sub"]);
            }
        )->first();

        if (isset($existing)) {
            throw new BadRequestException("User already registered");
        }

        return $data;
    }

    private static function createTokenInfo($at, array $res = []) {
        $res["token_kind"] = $at->name == "XM6TKN" ? "user" : "api";
        if ($at->name != "XM6TKN") {
            $res["app_id"] = $at->name;
            $res["abilities"] = $at->abilities;
        }
        $res["expire"] = isset($at->expire) ? date("Y-m-d H:i:s", strtotime($at->created_at) + $at->expire) : null;
        $res["must_reauth_at"] = isset($at->expire) && isset($at->revalidate) ? date("Y-m-d H:i:s", strtotime($at->created_at) + $at->revalidate) : null;
        return $res;
    }
}