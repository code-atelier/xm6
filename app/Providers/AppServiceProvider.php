<?php

namespace App\Providers;

use App\Models\Xm6PersonalAccessToken;
use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\Sanctum;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Sanctum::usePersonalAccessTokenModel(Xm6PersonalAccessToken::class);
        Sanctum::authenticateAccessTokensUsing(static function (Xm6PersonalAccessToken $accessToken, bool $is_valid) {
            return $is_valid && !$accessToken->hasExpired();
        });
    }
}
