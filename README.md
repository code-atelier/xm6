## About ExMachina 6

ExMachina 6 is a web platform that can manage multiple tenants, users and application in one package. External apps can access ExMachina 6 API to authenticate users and manage their licenses, while users are able to manage their tenant and application subscriptions.

ExMachina 6 is built on top of Laravel 9 web framework.

## Installation

### Pre-requisites
ExMachina 6 development and deployment requires:
- Composer - https://getcomposer.org/
- NPM - https://www.npmjs.com/

### Installing ExMachina 6
- Clone the repository: https://gitlab.com/code-atelier/xm6
- Open command prompt inside the cloned directory, and run the following commands in order:
  - `composer update` - this will download the required laravel files
  - `npm install` - this will download the required vue files
  - `npm run dev` - this will compile the vue app (you might have to run this twice for the first time)

## License

ExMachina 6 is licensed under MIT License.